#include "Collidable.hpp"

#include "Player.hpp"


std::vector<Collidable*> Collidable::m_collidables;


Collidable::Collidable()
{
	m_collidables.push_back(this);
}

Collidable::Collidable(sf::Sprite sprite, ResourceManager::ImageID image) :
	Object(sprite),
	m_image_id(image)
{
	m_collidables.push_back(this);
}

Collidable::~Collidable()
{
	for (std::vector<Collidable*>::iterator it = m_collidables.begin(); it != m_collidables.end(); it++)
	{
		if (*it == this)
		{
			m_collidables.erase(it);
			return;
		}
	}
}

bool Collidable::globalCollisionTest(const Player& player)
{
	for (const Collidable* collidable : m_collidables)
	{
		if (collidable != &player)
		{
			if (collidable->collides(player))
			{
				return true;
			}
		}
	}

	return false;
}

bool Collidable::collides(const Collidable& object) const
{
	sf::FloatRect intersection;

	if (this->m_sprite.getGlobalBounds().intersects(object.m_sprite.getGlobalBounds(), intersection))
	{
		std::shared_ptr<const sf::Image> image1 = ResourceManager::getImage(this->m_image_id);
		std::shared_ptr<const sf::Image> image2 = ResourceManager::getImage(object.m_image_id);

		if (this->m_sprite.getPosition().x < object.m_sprite.getPosition().x)
		{
			if (this->m_sprite.getPosition().y < object.m_sprite.getPosition().y)
			{
				const unsigned int start_x = intersection.left - this->m_sprite.getGlobalBounds().left;
				const unsigned int start_y = intersection.top - this->m_sprite.getGlobalBounds().top;

				for (unsigned int x = 0; x < intersection.width; x++)
				{
					for (unsigned int y = 0; y < intersection.height; y++)
					{
						if (image1->getPixel(x + start_x, y + start_y).a != 0 && image2->getPixel(x, y).a != 0)
						{
							return true;
						}
					}
				}
			}
			else
			{
				const unsigned int start_x = intersection.left - this->m_sprite.getGlobalBounds().left;
				const unsigned int start_y = intersection.top - object.m_sprite.getGlobalBounds().top;

				for (unsigned int x = 0; x < intersection.width; x++)
				{
					for (unsigned int y = 0; y < intersection.height; y++)
					{
						if (image1->getPixel(x + start_x, y).a != 0 && image2->getPixel(x, y + start_y).a != 0)
						{
							return true;
						}
					}
				}
			}
		}
		else
		{
			if (this->m_sprite.getPosition().y < object.m_sprite.getPosition().y)
			{
				const unsigned int start_x = intersection.left - object.m_sprite.getGlobalBounds().left;
				const unsigned int start_y = intersection.top - this->m_sprite.getGlobalBounds().top;

				for (unsigned int x = 0; x < intersection.width; x++)
				{
					for (unsigned int y = 0; y < intersection.height; y++)
					{
						if (image1->getPixel(x, y + start_y).a != 0 && image2->getPixel(x + start_x, y).a != 0)
						{
							return true;
						}
					}
				}
			}
			else
			{
				const unsigned int start_x = intersection.left - object.m_sprite.getGlobalBounds().left;
				const unsigned int start_y = intersection.top - object.m_sprite.getGlobalBounds().top;

				for (unsigned int x = 0; x < intersection.width; x++)
				{
					for (unsigned int y = 0; y < intersection.height; y++)
					{
						if (image1->getPixel(x, y).a != 0 && image2->getPixel(x + start_x, y + start_y).a != 0)
						{
							return true;
						}
					}
				}
			}
		}
	}

	return false;
}
