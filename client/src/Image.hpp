#pragma once

#include <SFML/Graphics/Sprite.hpp>

#include "Widget.hpp"


class Image : public Widget
{
public:
	Image(const sf::Sprite&);
	~Image();

	virtual void handleEvent(const sf::Event&);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	sf::Sprite m_sprite;
};