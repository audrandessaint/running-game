#include "Obstacle.hpp"

#include "Constantes.hpp"


Obstacle::Obstacle(sf::Sprite sprite, ResourceManager::ImageID image) :
	Collidable(sprite, image),
	m_width(sprite.getGlobalBounds().width)
{
	m_sprite.setScale(sf::Vector2f(SCALE, SCALE));
}

void Obstacle::move(float x, float y)
{
	m_sprite.move(x, y);
}

sf::Vector2f Obstacle::getPosition() const
{
	return m_sprite.getPosition();
}

void Obstacle::update(float dt)
{

}