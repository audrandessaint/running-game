#pragma once

#include <unordered_set>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/ConvexShape.hpp>


sf::ConvexShape generateRectangle(unsigned int, unsigned int);


class Widget : public sf::Drawable
{
public:
	Widget();
	Widget(const sf::Vector2f&);
	~Widget();

	virtual void setPosition(const sf::Vector2f&);
	void hide();
	void show();
	
	virtual void handleEvent(const sf::Event&) = 0;

protected:
	bool m_is_active;
	sf::Vector2f m_position;
};
