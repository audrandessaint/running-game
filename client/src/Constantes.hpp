#pragma once

#include <SFML/Graphics/Color.hpp>


#define SCALE 2
#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 700

#define FRAME_RATE 60

#define GRAVITY 100
#define INITIAL_JUMP_VELOCITY 40

#define GROUND_POSITION 500
#define INITIAL_GROUND_SPEED -3

#define DARK_COLOR sf::Color(24, 24, 24)
#define LIGHT_COLOR sf::Color(225, 216, 207)
#define CLEAR_COLOR LIGHT_COLOR

#define SERVER_IP_ADDRESS "shadowrunner.ddns.net" //"172.22.112.1"//"138.195.139.202"
#define SERVER_PORT 80

#define SCOREBOARD_MUSIC_PATH "resources/demacia-rising.flac" //"./ressources/victory - tow steps from hell.wav"
#define MAIN_MENU_MUSIC_PATH "resources/vex-the-gloomist.flac"
#define IN_GAME_MUSIC_PATH "resources/psyops.flac"