#pragma once

#include <vector>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/ConvexShape.hpp>

#include "Widget.hpp"
#include "Text.hpp"


class Leaderboard : public Widget
{
public:
	struct Entry
	{
		std::string username;
		std::string score;
	};

	Leaderboard(const sf::Vector2f&, const sf::Vector2f&);
	~Leaderboard();

	void setEntries(const std::vector<Entry>&);
	void addEntry(const Entry&);

	virtual void handleEvent(const sf::Event&);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	std::vector<Text> m_ranks;
	std::vector<Text> m_usernames;
	std::vector<Text> m_scores;
	float m_scrolling_speed = 12; //tmp
	float m_position;
	sf::View m_view;
	sf::ConvexShape m_border;
	sf::ConvexShape m_bar;
	sf::IntRect m_area;
};