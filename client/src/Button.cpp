#include "Button.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include "Constantes.hpp"
#include "ResourceManager.hpp"


Button::Button(const std::string& label, const sf::Vector2f& position, const sf::Vector2f& size) :
	Widget(position),
	m_is_bound_to_key(false),
	m_is_pushed_down(false),
	m_area(position, size)
{
	m_label.setFont(*ResourceManager::getFont(GAME_FONT));
	m_label.setFillColor(LIGHT_COLOR);
	setLabel(label);

	m_shape = generateRectangle(size.x, size.y);
	m_shape.setPosition(position);
	m_shape.setFillColor(DARK_COLOR);
}

void Button::setLabel(const std::string& label)
{
	m_label.setString(label);
	float x = (m_label.getGlobalBounds().width - m_area.width) / 2 - m_position.x;
	float y = (m_label.getGlobalBounds().height - m_area.height) / 2 - m_position.y + 7;
	m_label.setOrigin(x, y);
}

void Button::bindKey(sf::Keyboard::Key key)
{
	m_key = key;
	m_is_bound_to_key = true;
}

void Button::unbindKey()
{
	m_is_bound_to_key = false;
}

void Button::bindSound(ResourceManager::SoundBufferID sound_buffer)
{
	m_sound.setBuffer(*ResourceManager::getSoundBuffer(sound_buffer));
	m_is_audible = true;
}

void Button::bindCommand(const std::function<void(void)>& command)
{
	m_command = command;
}

void Button::handleEvent(const sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::MouseButtonPressed:
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			if (m_area.contains(event.mouseButton.x, event.mouseButton.y))
			{
				m_is_pushed_down = true;
				m_label.setFillColor(DARK_COLOR);
				m_shape.setFillColor(LIGHT_COLOR);
			}
		}
		break;
	case sf::Event::MouseButtonReleased:
		if (m_is_pushed_down)
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (m_area.contains(event.mouseButton.x, event.mouseButton.y))
				{
					if (m_is_audible)
					{
						m_sound.play();
					}

					m_command();
				}
			}
		}

		m_is_pushed_down = false;
		m_label.setFillColor(LIGHT_COLOR);
		m_shape.setFillColor(DARK_COLOR);
		break;
	case sf::Event::KeyPressed:
		if (m_is_bound_to_key)
		{
			if (event.key.code == m_key)
			{
				m_command();
			}
		}
		break;
	default:
		break;
	}
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_shape, states);
	target.draw(m_label, states);
}