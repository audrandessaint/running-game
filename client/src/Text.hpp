#pragma once

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

#include "Widget.hpp"
#include "ResourceManager.hpp"


// This class is merly a wrapper sf::Text to be used with the widget architecture/system of the game



class Text : public Widget
{
public:
	Text();
	Text(const sf::String&, const sf::Font&, unsigned int = 30);
	virtual ~Text();

	inline void setString(const sf::String& string) { m_text.setString(string); }
	void setFont(ResourceManager::FontID font);
	inline void setCharacterSize(unsigned int size) { m_text.setCharacterSize(size); }
	inline void setLineSpacing(float spacing) { m_text.setLineSpacing(spacing); }
	inline void setLetterSpacing(float spacing) { m_text.setLetterSpacing(spacing); }
	inline void setStyle(sf::Uint32 style) { m_text.setStyle(style); }
	inline void setFillColor(sf::Color color) { m_text.setFillColor(color); }
	inline void setOutlineColor(sf::Color color) { m_text.setOutlineColor(color); }
	inline void setOutlineThickness(float thickness) { m_text.setOutlineThickness(thickness); }

	inline const sf::String& getString() const { return m_text.getString(); }
	inline const sf::Font* getFont() const { return m_text.getFont(); }
	inline unsigned int getCharacterSize() const { return m_text.getCharacterSize(); }
	inline float getLetterSpacing() const { return m_text.getLetterSpacing(); }
	inline float getLineSpacing() const { return m_text.getLineSpacing(); }
	inline sf::Uint32 getStyle() const { return m_text.getStyle(); }
	inline const sf::Color& getFillColor() const { return m_text.getFillColor(); }
	inline const sf::Color& getOutlineColor() const { return m_text.getOutlineColor(); }
	inline float getOutlineThickness() const { return m_text.getOutlineThickness(); }
	inline sf::Vector2f findCharacterPos(std::size_t index) const { return m_text.findCharacterPos(index); }
	inline sf::FloatRect getLocalBounds() const { return m_text.getLocalBounds(); }
	inline sf::FloatRect getGlobalBounds() const { return m_text.getGlobalBounds(); }

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	void setPosition(const sf::Vector2f&);
	void setCenterPosition(const sf::Vector2f&);
	virtual void handleEvent(const sf::Event&);


private:
	sf::Text m_text;
};

