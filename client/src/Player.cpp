#include "Player.hpp"

#include "Constantes.hpp"
#include "ResourceManager.hpp"


Player::Player()
{
	FrameSeries fs(sf::IntRect(0, 0, 56, 32), sf::Vector2i(56, 0), 8);

	//code temporaire
	std::vector<ResourceManager::ImageID> tmp(fs.size());
	for (unsigned int i = 0; i < fs.size(); i++)
	{
		ResourceManager::ImageID id = ResourceManager::loadImageFromTexture(RUN_TEXTURE_ID, fs[i]);
		tmp[i] = id;
	}

	m_run_animation = Animation(RUN_TEXTURE_ID, fs, tmp);
	m_run_animation.setFrameRate(12);

	FrameSeries fs2(sf::IntRect(0, 0, 56, 32), sf::Vector2i(56, 0), 11);
	//code temporaire
	std::vector<ResourceManager::ImageID> tmp2(fs2.size());
	for (unsigned int i = 0; i < fs2.size(); i++)
	{
		ResourceManager::ImageID id = ResourceManager::loadImageFromTexture(JUMP_TEXTURE_ID, fs2[i]);
		tmp2[i] = id;
	}

	m_jump_animation = Animation(JUMP_TEXTURE_ID, fs2, tmp2);
	m_jump_animation.setFrameRate(12);

	m_current_animation = &m_run_animation;
	m_current_animation->start();
}

Player::Player(const Player& player)
{
	operator=(player);
}

void Player::operator=(const Player& player)
{
	this->m_position = player.m_position;
	this->m_image_id = player.m_image_id;
	this->m_sprite = player.m_sprite;
	this->m_is_jumping = player.m_is_jumping;
	this->m_jump_velocity = player.m_jump_velocity;
	this->m_run_animation = player.m_run_animation;
	this->m_jump_animation = player.m_jump_animation;

	this->m_current_animation = player.m_current_animation == &player.m_run_animation ? &this->m_run_animation : &this->m_jump_animation;
}

void Player::update(float dt)
{
	if (m_current_animation->reachedEnd())
	{
		changeAnimation(&m_run_animation);
	}

	if (m_is_jumping)
	{
		this->move(0, m_jump_velocity.y*0.35);
		m_jump_velocity += sf::Vector2f(0, GRAVITY) * dt;

		if (m_position.y >= 500 && m_jump_velocity.y > 0)
		{
			m_is_jumping = false;
			m_position.y = 500;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		jump();
	}

	changeSprite(m_current_animation->update(dt));
	m_image_id = m_current_animation->getCurrentRAMFrame();
}

void Player::jump()
{
	if (!m_is_jumping)
	{
		m_is_jumping = true;
		m_jump_velocity = sf::Vector2f(0, -INITIAL_JUMP_VELOCITY);
		changeAnimation(&m_jump_animation);
	}
}

void Player::changeAnimation(Animation* animation_ptr)
{
	m_current_animation = animation_ptr;
	m_current_animation->start();
}