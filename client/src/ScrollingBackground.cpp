#include "ScrollingBackground.hpp"

#include <random>

#include <SFML/Graphics/RenderTarget.hpp>

#include "Constantes.hpp"


ScrollingBackground::ScrollingBackground()
{
	sf::Sprite sprite2;
	sprite2.setTexture(*ResourceManager::getTexture(OBSTACLE_1_TEXTURE_ID));
	//sprite2.setPosition(801, 601);
	sprite2.setPosition(900, 501);

	auto ptr = std::shared_ptr<Obstacle>(new Obstacle(sprite2, OBSTACLE_1_IMAGE_ID));
	m_obstacles.push_back(ptr);
}

void ScrollingBackground::update(float dt)
{
	m_difficulty += dt;

	float ground_speed = (std::sqrt(m_difficulty)/10 + 1) * INITIAL_GROUND_SPEED;
	std::for_each(m_obstacles.begin(), m_obstacles.end(), [ground_speed](std::shared_ptr<Obstacle>& obstacle)->void { obstacle->move(ground_speed, 0); });

	//auto end = std::remove_if(m_obstacles.begin(), m_obstacles.end(), [](std::unique_ptr<Obstacle>& obstacle)->bool { return obstacle->getPosition().x + obstacle->getWidth() * SCALE < 0; });
	//m_obstacles.erase(end, m_obstacles.end());

	while (!m_obstacles.empty() && m_obstacles.front()->getPosition().x + m_obstacles.front()->getWidth() * SCALE < 0)
	{
		m_obstacles.pop_front();
	}

	if (numHiddenObstacles() < 1)
	{
		generateNextObstacle();
	}
}

bool ScrollingBackground::firstCriteria(float Ay, float By)
{
	return 2 * GRAVITY * std::max(Ay, By) < INITIAL_JUMP_VELOCITY * INITIAL_JUMP_VELOCITY;
}

float ScrollingBackground::calculateXmin(float Bx, float By, float vx)
{
	return Bx - vx * (INITIAL_JUMP_VELOCITY+ std::sqrt(INITIAL_JUMP_VELOCITY*INITIAL_JUMP_VELOCITY - 2 * GRAVITY*By)) / GRAVITY;
}

float ScrollingBackground::calculateXmax(float Ax, float Ay, float vx, float w)
{
	return Ax - w - vx * (INITIAL_JUMP_VELOCITY - std::sqrt(INITIAL_JUMP_VELOCITY*INITIAL_JUMP_VELOCITY - 2 * GRAVITY*Ay)) / GRAVITY;
}

float ScrollingBackground::calculateJumpLenght(float vx)
{
	return 2 * vx * INITIAL_JUMP_VELOCITY / GRAVITY;
}

unsigned int ScrollingBackground::numHiddenObstacles() const
{
	unsigned int result = 0;
	
	for (std::deque<std::shared_ptr<Obstacle>>::const_iterator it = m_obstacles.cbegin(); it != m_obstacles.cend(); it++)
	{
		if ((*it)->getPosition().x > WINDOW_WIDTH)
		{
			result++;
		}
	}

	return result;
}

void ScrollingBackground::generateNextObstacle()
{
	// choisir al�atoirement (selon une loi � d�terminer) le type d'obstacle + en fonction de la vitesse
	// trouver la position � partir de laquelle il est possible de placer cet obstacle
	// choisir al�atoirement (selon une loi � d�ternimer) l'emplacement de l'obstacle parmi les positions possibles
	// cr�er l'obstacle et l'ajouter � m_obstacles

	unsigned int e = (std::rand() % 10) * 50;

	sf::Sprite sprite2;
	sprite2.setTexture(*ResourceManager::getTexture(OBSTACLE_1_TEXTURE_ID));
	sprite2.setPosition(1400 + e, 501);

	auto ptr = std::shared_ptr<Obstacle>(new Obstacle(sprite2, OBSTACLE_1_IMAGE_ID));
	m_obstacles.push_back(ptr);
}

void ScrollingBackground::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (std::deque<std::shared_ptr<Obstacle>>::const_iterator it = m_obstacles.cbegin(); it != m_obstacles.cend(); it++)
	{
		target.draw(*(*it), states);
	}

	sf::RectangleShape rect;
	rect.setSize(sf::Vector2f(1200, 600));
	rect.setPosition(sf::Vector2f(0, 565));
	rect.setFillColor(sf::Color(24, 24, 24));
	target.draw(rect);
}