#include "Image.hpp"


Image::Image(const sf::Sprite& sprite) :
	m_sprite(sprite)
{
}

Image::~Image()
{
}

void Image::handleEvent(const sf::Event& event)
{
}

void Image::draw(sf::RenderTarget& target,sf::RenderStates states) const
{
	target.draw(m_sprite, states);
}