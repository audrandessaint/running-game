#pragma once

#include <SFML/Graphics.hpp>

#include "ResourceManager.hpp"


class Object : public sf::Drawable
{
public:
	Object();
	Object(sf::Sprite);
	virtual ~Object();

	void setPosition(float, float);
	void setPosition(const sf::Vector2f&);
	void move(float, float);
	void move(const sf::Vector2f&);

	virtual void update(float) = 0;
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

protected:
	void changeSprite(const sf::Sprite&);

	sf::Vector2f m_position;

	sf::Sprite m_sprite;
};