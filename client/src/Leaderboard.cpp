#include "Leaderboard.hpp"

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include "Constantes.hpp"


Leaderboard::Leaderboard(const sf::Vector2f& position, const sf::Vector2f& size) :
	Widget(position),
	m_view(sf::FloatRect(0, 0, size.x, size.y)),
	m_area(position.x, position.y, size.x, size.y),
	m_position(0)
{
	sf::FloatRect view_port(position.x / WINDOW_WIDTH, position.y / WINDOW_HEIGHT, size.x / WINDOW_WIDTH, size.y / WINDOW_HEIGHT);
	m_view.setViewport(view_port);

	m_border = generateRectangle(size.x, size.y);
	m_border.setPosition(position.x, position.y);
	m_border.setFillColor(sf::Color::Transparent);
	m_border.setOutlineThickness(SCALE);
	m_border.setOutlineColor(sf::Color(DARK_COLOR));

	float pos_x = size.x - 16;
	float pos_y = 2;
	float size_x = 14;
	float size_y = 100;
}

Leaderboard::~Leaderboard()
{

}

void Leaderboard::setEntries(const std::vector<Entry>& entries)
{
	m_ranks.clear();
	m_usernames.clear();
	m_scores.clear();

	for (const Entry& entry : entries)
	{
		addEntry(entry);
	}
}

void Leaderboard::addEntry(const Entry& entry)
{
	if (m_ranks.size() == 0)
	{
		Text rank;
		rank.setFont(GAME_FONT);
		rank.setString("Rank");
		rank.setFillColor(DARK_COLOR);
		rank.setPosition(sf::Vector2f(10, 40 * m_ranks.size() + 5));
		m_ranks.push_back(rank);

		Text username;
		username.setFont(GAME_FONT);
		username.setString("Username");
		username.setFillColor(DARK_COLOR);
		username.setPosition(sf::Vector2f(90, 40 * m_usernames.size() + 5));
		m_usernames.push_back(username);

		Text score;
		score.setFont(GAME_FONT);
		score.setString("Score");
		score.setFillColor(DARK_COLOR);
		score.setPosition(sf::Vector2f(400, 40 * m_scores.size() + 5));
		m_scores.push_back(score);
	}

	Text rank;
	rank.setFont(GAME_FONT);
	rank.setString(std::to_string(m_ranks.size()));
	rank.setFillColor(DARK_COLOR);
	rank.setPosition(sf::Vector2f(10, 40 * m_ranks.size() + 5));
	m_ranks.push_back(rank);

	Text username;
	username.setFont(GAME_FONT);
	username.setString(entry.username);
	username.setFillColor(DARK_COLOR);
	username.setPosition(sf::Vector2f(90, 40 * m_usernames.size() + 5));
	m_usernames.push_back(username);

	Text score;
	score.setFont(GAME_FONT);
	score.setString(entry.score);
	score.setFillColor(DARK_COLOR);
	score.setPosition(sf::Vector2f(400, 40 * m_scores.size() + 5));
	m_scores.push_back(score);

	float bar_size = m_area.height * m_area.height / (m_ranks.size() * 40);

	m_bar = generateRectangle(14, bar_size < m_area.height ? bar_size : 0);
	m_bar.setPosition(m_area.left + m_area.width - 16, m_area.top + 2);
	m_bar.setFillColor(DARK_COLOR);
}

void Leaderboard::handleEvent(const sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::EventType::MouseWheelScrolled:
		if (m_area.contains(event.mouseWheelScroll.x, event.mouseWheelScroll.y))
		{
			m_position = std::clamp(m_position - m_scrolling_speed * event.mouseWheelScroll.delta, 0.f, std::max(m_usernames.size() * 40.f - m_area.height, 0.f));
			m_view.setCenter(sf::Vector2f(0 + m_area.width/2, m_position + m_area.height/2));

			// error if no bar (case where there are to little entries)
			float bar_size = m_area.height * m_area.height / (m_ranks.size() * 40);
			float bar_position = m_area.top + 2 + (m_area.height - 2 - bar_size) * m_position / (m_usernames.size() * 40.f - m_area.height);
			m_bar.setPosition(m_area.left + m_area.width - 16, bar_position);
		}
		break;
	default:
		break;
	}
}

void Leaderboard::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.setView(m_view);
	for (unsigned int i = 0; i < m_usernames.size(); i++)
	{
		target.draw(m_ranks[i]);
		target.draw(m_usernames[i]);
		target.draw(m_scores[i]);
	}

	target.setView(target.getDefaultView());
	target.draw(m_bar);
	target.draw(m_border, states);
}