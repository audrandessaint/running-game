#pragma once

#include <memory>

#include "Animation.hpp"
#include "Collidable.hpp"



class Player : public Collidable
{
public:
	Player();
	Player(const Player&);

	void operator=(const Player&);

	void jump();
	
	virtual void update(float);
	
	void changeAnimation(Animation*);

private:
	bool m_is_jumping;
	sf::Vector2f m_jump_velocity;

	Animation* m_current_animation;

	Animation m_run_animation;
	Animation m_jump_animation;
};