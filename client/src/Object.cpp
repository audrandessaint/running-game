#include "Object.hpp"

#include "Constantes.hpp"


Object::Object()
{
}

Object::Object(sf::Sprite sprite) :
	m_sprite(sprite)
{
	m_sprite.setScale(sf::Vector2f(3, 3));
}

Object::~Object()
{
}

void Object::changeSprite(const sf::Sprite& sprite)
{
	m_sprite = sprite;
	m_sprite.setScale(sf::Vector2f(SCALE, SCALE));
	m_sprite.setPosition(m_position);
}

void Object::setPosition(float x, float y)
{
	setPosition(sf::Vector2f(x, y));
}

void Object::setPosition(const sf::Vector2f& new_position)
{
	m_position = new_position;
}

void Object::move(float x, float y)
{
	move(sf::Vector2f(x, y));
}

void Object::move(const sf::Vector2f& translation)
{
	m_position += translation;
}

void Object::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite, states);
}