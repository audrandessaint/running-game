#include "ResourceManager.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTexture.hpp>

#include "Constantes.hpp"


unsigned int ResourceManager::m_max_asset_id = MAX_ASSET_ID;

AssetManager<sf::Texture> ResourceManager::m_textureManager;
AssetManager<sf::Image> ResourceManager::m_imageManager;
AssetManager<sf::SoundBuffer> ResourceManager::m_soundManager;
AssetManager<sf::Font> ResourceManager::m_fontManager;


unsigned int ResourceManager::generateAssetID()
{
	return ++m_max_asset_id;
}

bool ResourceManager::loadTexture(TextureID id, const std::string& filename)
{
	sf::Texture texture;
	
	if (!texture.loadFromFile(filename))
	{
#ifdef _DEBUG
		std::cerr << "ResourceManager : failed to load '" << filename << "'" << std::endl;
#endif // _DEBUG

		return false;
	}

	m_textureManager.addAsset(id, std::make_shared<sf::Texture>(texture));
	updateMaxAssetID(id);
	return true;
}

bool ResourceManager::loadImage(ImageID id, const sf::Image& image)
{
	m_imageManager.addAsset(id, std::make_shared<sf::Image>(image));
	updateMaxAssetID(id);
	return true;
}

bool ResourceManager::loadImage(ImageID id, const std::string& filename)
{
	sf::Image image;

	if (!image.loadFromFile(filename))
	{
#ifdef _DEBUG
		std::cerr << "ResourceManager : failed to load '" << filename << "'" << std::endl;
#endif // _DEBUG

		return false;
	}

	m_imageManager.addAsset(id, std::make_shared<sf::Image>(image));
	updateMaxAssetID(id);
	return true;
}

bool ResourceManager::loadSoundBuffer(SoundBufferID id, const  std::string& filename)
{
	sf::SoundBuffer soundbuffer;

	if (!soundbuffer.loadFromFile(filename))
	{
#ifdef _DEBUG
		std::cerr << "ResourceManager : failed to load '" << filename << "'" << std::endl;
#endif // _DEBUG

		return false;
	}

	m_soundManager.addAsset(id, std::make_shared<sf::SoundBuffer>(soundbuffer));
	updateMaxAssetID(id);
	return true;
}

bool ResourceManager::loadFont(FontID id, const std::string& filename)
{
	sf::Font font;

	if (!font.loadFromFile(filename))
	{
#ifdef _DEBUG
		std::cerr << "ResourceManager : failed to load '" << filename << "'" << std::endl;
#endif
		return false;
	}

	m_fontManager.addAsset(id,std::make_shared<sf::Font>(font));
	updateMaxAssetID(id);
	return true;
}

void ResourceManager::unloadSoundBuffer(SoundBufferID id)
{
	m_soundManager.removeAsset(id);
}

void ResourceManager::unloadImage(ImageID id)
{
	m_imageManager.removeAsset(id);
}

void ResourceManager::unloadTexture(TextureID id)
{
	m_textureManager.removeAsset(id);
}

void ResourceManager::unloadFont(FontID id)
{
	m_fontManager.removeAsset(id);
}

std::shared_ptr<const sf::Texture> ResourceManager::getTexture(TextureID id)
{
	return m_textureManager.getAsset(id);
}

std::shared_ptr<const sf::Image> ResourceManager::getImage(ImageID id)
{
	return m_imageManager.getAsset(id);
}

std::shared_ptr<const sf::SoundBuffer> ResourceManager::getSoundBuffer(SoundBufferID id) 
{
	return m_soundManager.getAsset(id);
}

std::shared_ptr<const sf::Font> ResourceManager::getFont(FontID id)
{
	return m_fontManager.getAsset(id);
}

void ResourceManager::updateMaxAssetID(unsigned int id)
{
	if (id > m_max_asset_id)
	{
		m_max_asset_id = id;
	}
}

bool ResourceManager::loadImageFromTexture(ResourceManager::ImageID image_dst, ResourceManager::TextureID texture_scr)
{
	sf::Vector2u size = ResourceManager::getTexture(texture_scr)->getSize();
	return loadImageFromTexture(image_dst, texture_scr, sf::IntRect(0, 0, size.x, size.y));
}

bool ResourceManager::loadImageFromTexture(ResourceManager::ImageID image_dst, ResourceManager::TextureID texture_scr, const sf::IntRect& rectangle)
{
	sf::Sprite sprite;
	sprite.setTexture(*ResourceManager::getTexture(texture_scr));
	sprite.setTextureRect(rectangle);
	sprite.setScale(SCALE, SCALE);

	sf::RenderTexture target;
	target.create(sprite.getGlobalBounds().width, sprite.getGlobalBounds().height);
	target.draw(sprite);

	sf::Image result = target.getTexture().copyToImage();
	result.flipVertically();

	return ResourceManager::loadImage(image_dst, result);
}

ResourceManager::ImageID ResourceManager::loadImageFromTexture(ResourceManager::TextureID texture_scr)
{
	ResourceManager::ImageID id = ResourceManager::generateAssetID();
	loadImageFromTexture(id, texture_scr);
	return id;
}

ResourceManager::ImageID ResourceManager::loadImageFromTexture(ResourceManager::TextureID texture_scr, const sf::IntRect& rectangle)
{
	ResourceManager::ImageID id = ResourceManager::generateAssetID();
	loadImageFromTexture(id, texture_scr, rectangle);
	return id;
}