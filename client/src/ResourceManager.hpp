#pragma once

#include <memory>
#include <unordered_map>
#include <iostream>

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Audio/SoundBuffer.hpp>


#define RUN_TEXTURE_ID 0
#define ROCK_1_TEXTURE_ID 1
#define OBSTACLE_1_TEXTURE_ID 2
#define JUMP_TEXTURE_ID 6
#define LOGO_TEXTURE_ID 7

#define OBSTACLE_1_IMAGE_ID 3

#define GAME_FONT 4

#define CLICK_SOUND 5
#define GAME_OVER_SOUND 8

#define MAX_ASSET_ID 1000


template <typename T>
class AssetManager
{
public:
	void addAsset(unsigned int, std::shared_ptr<const T>);
	void removeAsset(unsigned int);

	std::shared_ptr<const T> getAsset(unsigned int);

private:
	std::unordered_map<unsigned int, std::shared_ptr<const T>> m_assets;
};


class ResourceManager
{
public:
	typedef unsigned int TextureID;
	typedef unsigned int ImageID;
	typedef unsigned int SoundBufferID;
	typedef unsigned int FontID;

	static unsigned int generateAssetID();

	static bool loadTexture(TextureID, const std::string&);
	static bool loadTexture(TextureID, const sf::Texture&);
	static void unloadTexture(TextureID);

	static bool loadImage(ImageID, const std::string&);
	static bool loadImage(ImageID, const sf::Image&);
	static void unloadImage(ImageID);

	static bool loadSoundBuffer(SoundBufferID, const std::string&);
	static bool loadSoundBuffer(SoundBufferID, const sf::SoundBuffer&);
	static void unloadSoundBuffer(SoundBufferID);

	static bool loadFont(FontID, const std::string&);
	static bool loadFont(FontID, const sf::Font&);
	static void unloadFont(FontID);

	static std::shared_ptr<const sf::Texture> getTexture(TextureID);
	static std::shared_ptr<const sf::Image> getImage(ImageID);
	static std::shared_ptr<const sf::SoundBuffer> getSoundBuffer(SoundBufferID);
	static std::shared_ptr<const sf::Font> getFont(FontID);

	static bool loadImageFromTexture(ResourceManager::ImageID, ResourceManager::TextureID);
	static bool loadImageFromTexture(ResourceManager::ImageID, ResourceManager::TextureID, const sf::IntRect&);
	static ResourceManager::ImageID loadImageFromTexture(ResourceManager::TextureID);
	static ResourceManager::ImageID loadImageFromTexture(ResourceManager::TextureID, const sf::IntRect&);

private:
	static void updateMaxAssetID(unsigned int);

	static AssetManager<sf::Texture> m_textureManager;
	static AssetManager<sf::Image> m_imageManager;
	static AssetManager<sf::SoundBuffer> m_soundManager;
	static AssetManager<sf::Font> m_fontManager;

	static unsigned int m_max_asset_id;
};


template<typename T>
void AssetManager<T>::addAsset(unsigned int id, std::shared_ptr<const T> asset)
{
#ifdef _DEBUG
	if (m_assets.find(id) != m_assets.end())
	{
		std::cerr << "RessourceManager : '" << id << "' already exists therefore the previous asset as been replaced" << std::endl;
	}
#endif // _DEBUG

	m_assets[id] = asset;
}

template<typename T>
void AssetManager<T>::removeAsset(unsigned int id)
{
	m_assets.erase(id);
}

template<typename T>
std::shared_ptr<const T> AssetManager<T>::getAsset(unsigned int id)
{
#ifdef _DEBUG
	if (m_assets.find(id) == m_assets.end())
	{
		std::cerr << "RessourceManager : '" << id << "' is not loaded" << std::endl;
	}
#endif // _DEBUG

	return m_assets[id];
}