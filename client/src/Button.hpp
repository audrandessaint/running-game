#pragma once

#include <functional>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Audio/Sound.hpp>

#include "Widget.hpp"
#include "ResourceManager.hpp"


class Button : public Widget
{
public:
	Button(const std::string&, const sf::Vector2f&, const sf::Vector2f&);

	void setLabel(const std::string&);

	void bindKey(sf::Keyboard::Key);
	void unbindKey();

	void bindSound(ResourceManager::SoundBufferID);

	void bindCommand(const std::function<void(void)>&);

	void handleEvent(const sf::Event&);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

protected:
	bool m_is_pushed_down;

	sf::Text m_label;
	sf::ConvexShape m_shape;
	sf::FloatRect m_area;

	bool m_is_audible;
	sf::Sound m_sound;

	bool m_is_bound_to_key;
	sf::Keyboard::Key m_key;

	std::function<void(void)> m_command;
};