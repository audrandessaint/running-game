#include "LoadingBar.hpp"

#include "Constantes.hpp"


LoadingBar::LoadingBar(unsigned int width, unsigned int height, float initial_progress) :
	m_width(width),
	m_height(height),
	m_border(generateRectangle(width, height))
{
	m_border.setOutlineThickness(SCALE);
	m_border.setOutlineColor(DARK_COLOR);
	m_border.setFillColor(LIGHT_COLOR);

	m_shape.setOutlineThickness(0);
	m_shape.setFillColor(DARK_COLOR);

	setProgress(initial_progress);
}

LoadingBar::~LoadingBar()
{
}

void LoadingBar::setProgress(float p)
{
	m_progress = std::clamp(p, 0.f, 1.f);
	m_shape = generateRectangle(m_width * m_progress, m_height);
	m_shape.setPosition(m_position);
	m_shape.setFillColor(DARK_COLOR);
}

void LoadingBar::update(float dp)
{
	setProgress(m_progress + dp);
}

void LoadingBar::setPosition(const sf::Vector2f& position)
{
	Widget::setPosition(position);

	m_shape.setPosition(position);
	m_border.setPosition(position);
}

void LoadingBar::handleEvent(const sf::Event& event)
{
}

void LoadingBar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_border, states);
	target.draw(m_shape, states);
}