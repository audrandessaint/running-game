#pragma once

#include<SFML/Graphics.hpp>

#include "ResourceManager.hpp"



class FrameSeries : public std::vector<sf::IntRect>
{
public:
	FrameSeries();
	FrameSeries(const sf::IntRect&, const sf::Vector2i&, unsigned int);
};

FrameSeries merge(const FrameSeries&, const FrameSeries&);


class Animation
{
public:
	Animation() {};
	Animation(ResourceManager::TextureID, const FrameSeries&, const std::vector<ResourceManager::ImageID>&);

	void setFrameRate(float);

	void start();
	void stop();

	bool reachedEnd() const;

	const sf::Sprite& update(float);

	const sf::Sprite& getCurrentFrame() const;
	ResourceManager::ImageID getCurrentRAMFrame() const;

private:
	unsigned int m_counter;
	bool m_reached_end;

	float m_framerate_inv;
	float m_elapsed_time;

	FrameSeries m_frame_series;

	ResourceManager::TextureID m_texture;
	std::vector<ResourceManager::ImageID> m_RAM_frames;

	sf::Sprite m_sprite;
};

