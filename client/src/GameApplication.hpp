#pragma once

#include <memory>
#include <set>
#include <future>

#include <openssl/ssl.h>
#include <SFML/Audio/Music.hpp>

#include "Widget.hpp"
#include "Object.hpp"
#include "TextField.hpp"
#include "Text.hpp"
#include "LoadingBar.hpp"
#include "Player.hpp"
#include "ResourceManager.hpp"


class GameApplication;


class GameState
{
public:
	GameState(GameApplication*);
	virtual ~GameState();

	virtual void init();
	virtual void exit();

	virtual void handleEvent(const sf::Event&);
	virtual void update(float);

protected:
	GameApplication* m_game;

	std::set<std::shared_ptr<Object>> m_objects;
	std::set<std::shared_ptr<Widget>> m_widgets;
};


class LoadingScreen : public GameState
{
public:
	LoadingScreen(GameApplication*);
	virtual ~LoadingScreen();

	virtual void init();
	virtual void exit();

	virtual void update(float);

private:
	void loadResources();

	std::shared_ptr<LoadingBar> m_progress_bar;
	std::thread m_thread;
};


class LoginScreen : public GameState
{
public:
	LoginScreen(GameApplication*);
	virtual ~LoginScreen();

	virtual void init();

private:
	std::shared_ptr<TextField> m_username_textfield;
	std::shared_ptr<TextField> m_password_textfield;

	bool checkConnexion(const std::string&, const std::string&, std::string&);
	void loginAttempt();
};


class NewAccountPage : public GameState
{
public:
	NewAccountPage(GameApplication*);
	virtual ~NewAccountPage();

	void init();

private:
	std::shared_ptr<TextField> m_username_textfield;
	std::shared_ptr<TextField> m_password_textfield;
	std::shared_ptr<TextField> m_email_textfield;

	bool createNewAccount();
};


class MainMenu : public GameState
{
public:
	MainMenu(GameApplication*);
	~MainMenu();

	void init();
};

class MainScene : public GameState
{
public:
	MainScene(GameApplication*);
	virtual ~MainScene();

	virtual void init();
	virtual void exit();

	virtual void update(float);
	virtual void handleEvent(const sf::Event&);

	inline void setUsername(const std::string& username) { m_username = username; }

private:
	std::shared_ptr<Player> m_player_ptr;
	float m_ellapsed_time;
	unsigned int m_score;
	std::string m_username;
	std::shared_ptr<Text> m_score_print;
};

class GameOverScreen : public GameState
{
public:
	GameOverScreen(GameApplication*);
	virtual ~GameOverScreen();

	virtual void init() {}
	void init(unsigned int, bool);

private:
	sf::Sound m_sound;
};

class Scoreboard : public GameState
{
public:
	Scoreboard(GameApplication*);
	virtual ~Scoreboard();

	virtual void init();
	virtual void exit();
};

class OptionMenu : public GameState
{
public:
	OptionMenu(GameApplication*);
	virtual ~OptionMenu();

	virtual void init();

	virtual void handleEvent(const sf::Event&);
};


class GameApplication
{
public:
	friend GameState;

	GameApplication();
	~GameApplication();

	void init();
	void run();

	bool send(const char*);
	bool receive(char*, unsigned int, unsigned int&);

	bool updateScore(unsigned int);

	inline void setToken(const std::string& token) { m_token = token; }
	inline void setUsername(const std::string& username) { m_main_scene.setUsername(username); }
	inline void setConnection(SSL_CTX* ctx, BIO* bio, SSL* ssl) { m_context = ctx; m_bio = bio; m_ssl = ssl; }
	inline void setMusic(const std::string&);

	inline std::string getToken() const { return m_token; }

	void switchTo(GameState*);

	void switchToLoginScreen();
	void switchToMainScene();
	void switchToGameOverScreen(unsigned int);
	void switchToScoreBoard();
	void switchToOptionMenu();
	void switchToMainMenu();
	void switchToNewAccountPage();

private:
	std::string m_token;

	SSL_CTX* m_context;
	BIO* m_bio;
	SSL* m_ssl;

	sf::Music m_music;

	LoadingScreen m_loading_screen;
	LoginScreen m_login_screen;
	NewAccountPage m_new_account_page;
	MainMenu m_main_menu;
	MainScene m_main_scene;
	GameOverScreen m_game_over_screen;
	Scoreboard m_scoreboard;
	OptionMenu m_option_menu;

	GameState* m_current_state;

	sf::RenderWindow m_window;
	sf::Clock m_clock;
};

