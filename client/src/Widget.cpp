#include "Widget.hpp"

#include "Constantes.hpp"


sf::ConvexShape generateRectangle(unsigned int width, unsigned int height)
{
	if (width < SCALE || height < SCALE)
	{
		return sf::ConvexShape();
	}

	sf::ConvexShape result;
	result.setPointCount(12);
	result.setPoint(0, sf::Vector2f(SCALE, SCALE));
	result.setPoint(1, sf::Vector2f(SCALE, 0));
	result.setPoint(2, sf::Vector2f(width - SCALE, 0));
	result.setPoint(3, sf::Vector2f(width - SCALE, SCALE));
	result.setPoint(4, sf::Vector2f(width, SCALE));
	result.setPoint(5, sf::Vector2f(width, height - SCALE));
	result.setPoint(6, sf::Vector2f(width - SCALE, height - SCALE));
	result.setPoint(7, sf::Vector2f(width - SCALE, height));
	result.setPoint(8, sf::Vector2f(SCALE, height));
	result.setPoint(9, sf::Vector2f(SCALE, height - SCALE));
	result.setPoint(10, sf::Vector2f(0, height - SCALE));
	result.setPoint(11, sf::Vector2f(0, SCALE));
	return result;
}


Widget::Widget() :
	m_is_active(true)
{
}

Widget::Widget(const sf::Vector2f& position) :
	m_is_active(true),
	m_position(position)
{
}

Widget::~Widget()
{
}

void Widget::setPosition(const sf::Vector2f& position)
{
	m_position = position;
}

void Widget::hide()
{
	m_is_active = false;
}

void Widget::show()
{
	m_is_active = true;
}