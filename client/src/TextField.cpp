#include "TextField.hpp"

#include "Constantes.hpp"


TextField::TextField(const sf::Vector2f& position, const sf::Vector2f& size, bool hidden) :
	Button("", position, size),
	m_is_selected(false),
	m_is_hidden(hidden)
{
	m_command = std::bind(&TextField::switchSelectState, this);

	m_shape.setFillColor(LIGHT_COLOR);
	m_shape.setOutlineThickness(SCALE);
	m_shape.setOutlineColor(sf::Color(DARK_COLOR));
}

TextField::TextField(const TextField& text_field) :
	TextField(text_field.m_position, sf::Vector2f(text_field.m_area.width, text_field.m_area.height), text_field.m_is_hidden)
{
	m_is_selected = text_field.m_is_selected;
	m_string = text_field.m_string;
	m_label = text_field.m_label;
}

TextField::~TextField()
{
}

sf::String TextField::getText()
{
	return m_string;
}

void TextField::setText(const sf::String& string)
{
	m_string = string;

	if (m_is_hidden)
	{
		std::string label;
		for (unsigned int i = 0; i < m_string.getSize(); i++)
		{
			label.append("*");
		}
		setLabel(label);
	}
	else
	{
		setLabel(string);
	}
	
}

void TextField::handleEvent(const sf::Event& event)
{
	//Button::handleEvent(event);
	m_shape.setFillColor(LIGHT_COLOR);
	m_label.setFillColor(DARK_COLOR);

	switch (event.type)
	{
	case sf::Event::MouseButtonPressed:
		if (m_area.contains(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)))
		{
			m_is_selected = true;
			m_shape.setOutlineThickness(SCALE * 2);
		}
		else
		{
			m_is_selected = false;
			m_shape.setOutlineThickness(SCALE);
		}
		break;
	case sf::Event::TextEntered:
		if (m_is_selected)
		{
			if (event.text.unicode == '\b')
			{
				if (!m_string.isEmpty())
				{
					m_string.erase(m_string.getSize() - 1, 1);
				}
			}
			else
			{
				m_string += event.text.unicode;

				//valeur -10 arbitraire pour eviter le dépassement, voir une valeur générale en fonction de SCALE
				if (m_label.getGlobalBounds().width > m_area.width - 30)
				{
					m_string.erase(m_string.getSize() - 1, 1);
				}
			}

			setText(m_string);
		}
		break;
	default:
		break;
	}
}

void TextField::switchSelectState()
{
	this->m_is_selected = true;
}