#pragma once

#include <SFML/Graphics/Text.hpp>

#include "Button.hpp"


// changer l'h�ritage ici, cette classe devrait h�riter de Widget et non de Button,
// trouver une autre mani�re de factoriser le code (notamment pour la forme du widget
// qui est �galement utilis�e plusieurs fois dans la classe LeaderBoard)
class TextField : public Button
{
public:
	TextField(const sf::Vector2f&, const sf::Vector2f&, bool = false);
	TextField(const TextField&);
	~TextField();

	sf::String getText();
	void setText(const sf::String&);

	virtual void handleEvent(const sf::Event&);

	//virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	void switchSelectState();

	bool m_is_selected;
	bool m_is_hidden;

	sf::String m_string;
};

