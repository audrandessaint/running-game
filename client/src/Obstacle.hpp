#pragma once

#include "Object.hpp"
#include "Collidable.hpp"
#include "ResourceManager.hpp"


class Obstacle : public Collidable
{
public:
	Obstacle(sf::Sprite, ResourceManager::ImageID);
	virtual ~Obstacle() {}

	void move(float, float);
	sf::Vector2f getPosition() const;
	inline float getWidth() const { return m_width; }

	virtual void update(float);

private:
	float m_width;
};

