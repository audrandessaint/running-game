#include "GameApplication.hpp"

#include <string>
#include <sstream>
#include <chrono>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/document.h>

#include <openssl/err.h>

#include "Constantes.hpp"
#include "Button.hpp"
#include "Text.hpp"
#include "TextField.hpp"
#include "Image.hpp"
#include "Player.hpp"
#include "Leaderboard.hpp"
#include "ScrollingBackground.hpp"
#include "ResourceManager.hpp"


// main menu*  : https://www.youtube.com/watch?v=z1QrEf8Ti_0&list=PLJP5_qSxMbkJvmDlvP8v_tZ5Lc8Fzginj&index=9
// main menu : https://www.youtube.com/watch?v=NSQZqVsaKWY
// main menu : https://www.youtube.com/watch?v=mhkQ4NaNeOA
// in game : https://www.youtube.com/watch?v=Gm2S3MyzgFQ&list=PLJP5_qSxMbkJvmDlvP8v_tZ5Lc8Fzginj&index=25
// in game* : https://www.youtube.com/watch?v=VWUk-ZMK8w4&list=PLJP5_qSxMbkJvmDlvP8v_tZ5Lc8Fzginj&index=26
// score board* : https://www.youtube.com/watch?v=8aluBPpzCHc&list=PLJP5_qSxMbkL5NiX2W8pm-Wf07GJpTkaW
// score board x : https://www.youtube.com/watch?v=nmgH1ZSBv3w&list=PLJP5_qSxMbkL5NiX2W8pm-Wf07GJpTkaW&index=5
// score board : https://www.youtube.com/watch?v=ADgvSi7SgB4


GameState::GameState(GameApplication* game) :
	m_game(game)
{
}

GameState::~GameState()
{
}

void GameState::init()
{
	m_objects.clear();
	m_widgets.clear();
}

void GameState::exit()
{
}

void GameState::handleEvent(const sf::Event& event)
{
	for (std::shared_ptr<Widget> widget : m_widgets)
	{
		widget->handleEvent(event);
	}
}

void GameState::update(float dt)
{
	for (std::shared_ptr<Object> object : m_objects)
	{
		object->update(dt);
		m_game->m_window.draw(*object);
	}

	for (std::shared_ptr<Widget> widget : m_widgets)
	{
		m_game->m_window.draw(*widget);
	}
}


LoadingScreen::LoadingScreen(GameApplication* game) :
	GameState(game)
{
	ResourceManager::loadTexture(LOGO_TEXTURE_ID, "resources/character.png");
	sf::Sprite sprite(*ResourceManager::getTexture(LOGO_TEXTURE_ID));
	sprite.scale(sf::Vector2f(10.f, 10.f));
	sf::FloatRect bounds = sprite.getGlobalBounds();
	sprite.setPosition((WINDOW_WIDTH - bounds.width) / 2, (WINDOW_HEIGHT - bounds.height) / 2);

	Image logo(sprite);
	m_widgets.insert(std::make_shared<Image>(logo));

	m_progress_bar = std::make_shared<LoadingBar>(300, 20);
	m_progress_bar->setPosition(sf::Vector2f((WINDOW_WIDTH - 300) / 2, (WINDOW_HEIGHT - 20) / 2 + 250));
	m_widgets.insert(m_progress_bar);
}

LoadingScreen::~LoadingScreen()
{
}

void LoadingScreen::init()
{
	m_thread = std::thread(std::bind(&LoadingScreen::loadResources, this));
}

void LoadingScreen::exit()
{
	GameState::exit();
	m_game->setMusic(MAIN_MENU_MUSIC_PATH);
}

void LoadingScreen::update(float dt)
{
	GameState::update(dt);

	if (m_progress_bar->getProgress() == 1.f)
	{
		m_thread.join();
		m_game->switchToLoginScreen();
	}
}

void LoadingScreen::loadResources()
{
	std::cout << "Loading ressources...";
	//ResourceManager::loadFont(GAME_FONT, "ressources/Billy-Regular.ttf");
	ResourceManager::loadFont(GAME_FONT, "resources/Bullette.ttf");
	m_progress_bar->update(0.07f);
	ResourceManager::loadTexture(RUN_TEXTURE_ID, "resources/run2.png");
	m_progress_bar->update(0.07f);
	ResourceManager::loadTexture(OBSTACLE_1_TEXTURE_ID, "resources/obstacle1.png");
	m_progress_bar->update(0.07f);
	ResourceManager::loadImageFromTexture(OBSTACLE_1_IMAGE_ID, OBSTACLE_1_TEXTURE_ID);
	m_progress_bar->update(0.07f);
	ResourceManager::loadTexture(JUMP_TEXTURE_ID, "resources/jump_test.png");
	m_progress_bar->update(0.07f);
	ResourceManager::loadSoundBuffer(CLICK_SOUND, "resources/click.wav");
	m_progress_bar->update(0.07f);
	ResourceManager::loadSoundBuffer(GAME_OVER_SOUND, "resources/game-over-2.wav");
	m_progress_bar->update(0.07f);

	std::cout << "\r[done] Ressources loaded" << std::endl;
	std::cout << "Connecting to the server...";

	const SSL_METHOD* method = TLSv1_2_client_method();

	if (method == NULL)
	{
		//handle error
		std::cout << "1" << std::endl;
		return;
	}

	SSL_CTX* context = SSL_CTX_new(method);

	if (context == NULL)
	{
		// handle error
		std::cout << "2" << std::endl;
		return;
	}

	BIO* bio = BIO_new_ssl_connect(context);

	if (bio == NULL)
	{
		// handle error
		std::cout << "3" << std::endl;
		return;
	}

	std::string server_addr = std::string(SERVER_IP_ADDRESS) + ":" + std::to_string(SERVER_PORT);

	SSL* ssl = NULL;
	BIO_get_ssl(bio, &ssl);
	SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
	BIO_set_conn_hostname(bio, server_addr.c_str());

	m_progress_bar->update(0.1f);

	if (BIO_do_connect(bio) <= 0)
	{
		BIO* bio = BIO_new(BIO_s_mem());
		ERR_print_errors(bio);
		char* buf = nullptr;
		size_t len = BIO_get_mem_data(bio, &buf);
		std::cout << std::string(buf, len) << std::endl;
		BIO_free(bio);
		//free(buf);


		// handle error
		std::cout << "Failed to connect to the server" << std::endl;
		return;
	}

	////////////////// CERTIFICATE VERIFICATION HERE /////////////////////////

	/*BIO* bio_mem = BIO_new(BIO_s_mem());
	BIO_puts(bio_mem, pem_c_str);
	X509* x509 = PEM_read_bio_X509(bio_mem, NULL, NULL, NULL);

	EVP_PKEY* pkey = X509_get_pubkey(x509);
	int r = X509_verify(x509, pkey);
	EVP_PKEY_free(pkey);

	BIO_free(bio_mem);
	X509_free(x509);*/

	m_progress_bar->setProgress(1.f);
	std::cout << "\r[done] Connected to the server" << std::endl;
	m_game->setConnection(context, bio, ssl);
}

LoginScreen::LoginScreen(GameApplication* game) :
	GameState(game)
{
}

LoginScreen::~LoginScreen()
{
}

void LoginScreen::init()
{
	GameState::init();

	Text username_label("username", *ResourceManager::getFont(GAME_FONT));
	username_label.setFillColor(DARK_COLOR);
	username_label.setPosition(sf::Vector2f(400, 160));
	m_widgets.insert(std::make_shared<Text>(username_label));

	TextField username_textfield(sf::Vector2f(400, 200), sf::Vector2f(400, 45));
	m_username_textfield = std::make_shared<TextField>(username_textfield);
	m_widgets.insert(m_username_textfield);

	Text password_label("password", *ResourceManager::getFont(GAME_FONT));
	password_label.setFillColor(DARK_COLOR);
	password_label.setPosition(sf::Vector2f(400, 260));
	m_widgets.insert(std::make_shared<Text>(password_label));

	TextField password_textfield(sf::Vector2f(400, 300), sf::Vector2f(400, 45), true);
	m_password_textfield = std::make_shared<TextField>(password_textfield);
	m_widgets.insert(m_password_textfield);

	Button login_button("LOGIN", sf::Vector2f(400, 400), sf::Vector2f(400, 50));
	login_button.bindCommand(std::bind(&LoginScreen::loginAttempt, this));
	login_button.bindKey(sf::Keyboard::Enter);
	login_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(login_button));

	Button new_account_button("NEW ACCOUNT", sf::Vector2f(400, 470), sf::Vector2f(400, 50));
	new_account_button.bindCommand(std::bind(&GameApplication::switchToNewAccountPage, m_game));
	new_account_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(new_account_button));
}

static std::vector<std::string> splitString(std::string string, const std::string& delimiter)
{
	std::vector<std::string> result;	
	std::size_t last = 0;
	std::size_t next = 0;
	
	while(true)
	{
		next = string.find(delimiter, last);
		if (next == std::string::npos) break;
		result.push_back(string.substr(last, next - last));
		last = next + delimiter.size();
	}

	result.push_back(string.substr(last));
	return result;
}

bool LoginScreen::checkConnexion(const std::string& username, const std::string& password, std::string& token)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	writer.StartObject();
	writer.Key("username"); writer.String(username.c_str());
	writer.Key("password"); writer.String(password.c_str());
	writer.EndObject();

	std::string request = "LOGIN\n" + std::string(buffer.GetString());
	bool status = m_game->send(request.c_str());

	if (!status)
	{
		std::cout << "Failed to send the data" << std::endl;
		return false;
	}

	unsigned int size;
	char response[1000];
	status = m_game->receive(response, 1000, size);

	if (!status)
	{
		std::cout << "Failed to receive the data" << std::endl;
		return false;
	}

	std::string s_response(response, size);
	std::vector<std::string> ss_response = splitString(s_response, "\n");

	if (ss_response[0] == "OK" && ss_response.size() == 2)
	{
		rapidjson::Document document;
		document.Parse(ss_response[1].c_str());

		if (!document.HasMember("token"))
		{
			return false;
		}

		token = document["token"].GetString();
		return true;
	}

	if (ss_response[0] == "FAIL")
	{
		// tmp -> in the future replace with the error message of the response displayed in game
		std::cout << "Authentication failed" << std::endl;
		return false;
	}
	
	std::cout << "Unexpected response from the server" << std::endl;
	return false;
}

void LoginScreen::loginAttempt()
{
	std::string token;
	if (checkConnexion(m_username_textfield->getText(), m_password_textfield->getText(), token))
	{
		m_game->setToken(token);
		m_game->setUsername(m_username_textfield->getText());
		m_game->switchToMainMenu();
	}
}


NewAccountPage::NewAccountPage(GameApplication* game):
	GameState(game)
{
}

NewAccountPage::~NewAccountPage()
{
}

void NewAccountPage::init()
{
	GameState::init();

	Text username;
	username.setString("Username");
	username.setCenterPosition(sf::Vector2f(400, 160));
	username.setFont(GAME_FONT);
	username.setFillColor(DARK_COLOR);
	m_widgets.insert(std::make_shared<Text>(username));

	TextField username_textfield(sf::Vector2f(400, 200), sf::Vector2f(400, 45));
	m_username_textfield = std::make_shared<TextField>(username_textfield);
	m_widgets.insert(m_username_textfield);

	Text password;
	password.setString("Password");
	password.setCenterPosition(sf::Vector2f(400, 260));
	password.setFont(GAME_FONT);
	password.setFillColor(DARK_COLOR);
	m_widgets.insert(std::make_shared<Text>(password));

	TextField password_textfield(sf::Vector2f(400, 300), sf::Vector2f(400, 45));
	m_password_textfield = std::make_shared<TextField>(password_textfield);
	m_widgets.insert(m_password_textfield);

	Text email;
	email.setString("Email");
	email.setCenterPosition(sf::Vector2f(400, 360));
	email.setFont(GAME_FONT);
	email.setFillColor(DARK_COLOR);
	m_widgets.insert(std::make_shared<Text>(email));

	TextField email_textfield(sf::Vector2f(400, 400), sf::Vector2f(400, 45));
	m_email_textfield = std::make_shared<TextField>(email_textfield);
	m_widgets.insert(m_email_textfield);

	Button create_new_account_button("Create new account", sf::Vector2f(400, 480), sf::Vector2f(400, 50));
	create_new_account_button.bindCommand(std::bind(&NewAccountPage::createNewAccount, this));
	create_new_account_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(create_new_account_button));

	Button exit_button("Exit", sf::Vector2f(400, 550), sf::Vector2f(400, 50));
	exit_button.bindCommand(std::bind(&GameApplication::switchToLoginScreen, m_game));
	exit_button.bindKey(sf::Keyboard::Escape);
	exit_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(exit_button));
}

bool NewAccountPage::createNewAccount()
{
	std::string username = m_username_textfield->getText();
	std::string password = m_password_textfield->getText();
	std::string email = m_email_textfield->getText();

	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	writer.StartObject();
	writer.Key("username"); writer.String(username.c_str());
	writer.Key("password"); writer.String(password.c_str());
	writer.Key("email"); writer.String(email.c_str());
	writer.EndObject();

	std::string request = "CREATEUSER\n" + std::string(buffer.GetString());
	bool status = m_game->send(request.c_str());

	if (!status)
	{
		std::cerr << "Failed to send the data" << std::endl;
		return false;
	}

	unsigned int size;
	char answer[200];
	status = m_game->receive(answer, 200, size);

	if (!status)
	{
		std::cout << "Failed to receive the data" << std::endl;
	}

	if (std::string(answer, size) == "OK")
	{
		m_game->switchToLoginScreen();
	}
}


MainMenu::MainMenu(GameApplication* game) :
	GameState(game)
{
}

MainMenu::~MainMenu()
{
}

void MainMenu::init()
{
	GameState::init();

	Button play_button("Play", sf::Vector2f(500, 200), sf::Vector2f(200, 50));
	play_button.bindCommand(std::bind(&GameApplication::switchToMainScene, m_game));
	play_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(play_button));

	Button ranking_button("Ranking", sf::Vector2f(500, 280), sf::Vector2f(200, 50));
	ranking_button.bindCommand(std::bind(&GameApplication::switchToScoreBoard, m_game));
	ranking_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(ranking_button));

	Button option_button("Options", sf::Vector2f(500, 360), sf::Vector2f(200, 50));
	option_button.bindCommand(std::bind(&GameApplication::switchToOptionMenu, m_game));
	option_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(option_button));

	Button logout_button("Logout", sf::Vector2f(500, 440), sf::Vector2f(200, 50));
	logout_button.bindCommand(std::bind(&GameApplication::switchToLoginScreen, m_game));
	logout_button.bindSound(CLICK_SOUND);
	m_widgets.insert(std::make_shared<Button>(logout_button));
}

MainScene::MainScene(GameApplication* game):
	GameState(game)
{
}

MainScene::~MainScene()
{
}

void MainScene::init()
{
	GameState::init();
	m_ellapsed_time = 0;

	ScrollingBackground scrolling_background;
	m_objects.insert(std::make_shared<ScrollingBackground>(scrolling_background));

	Player player;
	player.setPosition(550, 500);
	m_player_ptr = std::make_shared<Player>(player);
	m_objects.insert(m_player_ptr);

	Text score_print;
	score_print.setPosition(sf::Vector2f(20, 20));
	score_print.setString("test");
	score_print.setFont(GAME_FONT);
	score_print.setFillColor(DARK_COLOR);
	m_score_print = std::make_shared<Text>(score_print);
	m_widgets.insert(m_score_print);

	Text username_print;
	username_print.setPosition(sf::Vector2f(WINDOW_WIDTH - 110, 20));
	username_print.setString(m_username);
	username_print.setFont(GAME_FONT);
	username_print.setFillColor(DARK_COLOR);
	m_widgets.insert(std::make_shared<Text>(username_print));

	m_game->setMusic(IN_GAME_MUSIC_PATH);
}

void MainScene::exit()
{
	GameState::exit();
	m_game->setMusic(MAIN_MENU_MUSIC_PATH);
	m_game->updateScore(m_score);
}

void MainScene::update(float dt)
{
	GameState::update(dt);

	m_ellapsed_time += dt;
	m_score = static_cast<unsigned int>((m_ellapsed_time / 2)*(m_ellapsed_time / 10));

	m_score_print->setString(sf::String("Score: ") + sf::String(std::to_string(m_score)));

	if (Collidable::globalCollisionTest(*m_player_ptr))
	{
		m_game->switchToGameOverScreen(m_score);
	}
}

void MainScene::handleEvent(const sf::Event& event)
{
	GameState::handleEvent(event);

	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Escape:
			m_game->switchToMainMenu();
			break;
		}
		break;
	}
}

GameOverScreen::GameOverScreen(GameApplication* game) :
	GameState(game)
{
}

GameOverScreen::~GameOverScreen()
{
}

void GameOverScreen::init(unsigned int score, bool new_best)
{
	GameState::init();

	std::string s_score = "Score ";
	if (new_best)
	{
		s_score = "New best score ";
	}

	Text go_text;
	go_text.setFont(GAME_FONT);
	go_text.setString("Game Over");
	go_text.setCharacterSize(120);
	go_text.setFillColor(DARK_COLOR);
	go_text.setCenterPosition(sf::Vector2f(WINDOW_WIDTH/2, WINDOW_HEIGHT/2 - 85));
	m_widgets.insert(std::make_shared<Text>(go_text));

	Text score_text;
	score_text.setFont(GAME_FONT);
	score_text.setString(s_score + std::to_string(score));
	score_text.setFillColor(DARK_COLOR);
	score_text.setCenterPosition(sf::Vector2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 + 20));
	m_widgets.insert(std::make_shared<Text>(score_text));

	Button play_button("Play Again", sf::Vector2f(500, 420), sf::Vector2f(200, 50));
	play_button.bindCommand(std::bind(&GameApplication::switchToMainScene, m_game));
	play_button.bindSound(CLICK_SOUND);
	play_button.bindKey(sf::Keyboard::Enter);
	m_widgets.insert(std::make_shared<Button>(play_button));

	Button button("Main Menu", sf::Vector2f(500, 490), sf::Vector2f(200, 50));
	button.bindCommand(std::bind(&GameApplication::switchToMainMenu, m_game));
	button.bindSound(CLICK_SOUND);
	button.bindKey(sf::Keyboard::Escape);
	m_widgets.insert(std::make_shared<Button>(button));

	m_sound.setBuffer(*ResourceManager::getSoundBuffer(GAME_OVER_SOUND));
	m_sound.play();
}

Scoreboard::Scoreboard(GameApplication* game) :
	GameState(game)
{
}

Scoreboard::~Scoreboard()
{
}

void Scoreboard::init()
{
	GameState::init();

	bool result = m_game->send(("SCOREBOARD " + m_game->getToken()).c_str());

	if (!result)
	{
		std::cout << "Failed to send the request" << std::endl;
	}

	char* buffer = (char*)malloc(4000);

	if (buffer == nullptr)
	{
		std::cout << "Failed to allocate memory" << std::endl;
		//tmp
		m_game->switchToMainMenu();
		return;
	}

	unsigned int received = 0;
	result = m_game->receive(buffer, 4000, received);

	if (!result)
	{
		std::cout << "Failed to receive the leader board" << std::endl;
	}

	std::string response(buffer, received); free(buffer);
	std::vector<std::string> s_response = splitString(response, "\n");

	if (s_response[0] != "OK" || s_response.size() != 2)
	{
		std::cout << "Server side fail" << std::endl;
	}

	std::cout << s_response[0] << std::endl;
	std::cout << s_response[1] << std::endl;
	
	rapidjson::Document document;
	document.Parse(s_response[1].c_str());

	Text leader_board_label;
	leader_board_label.setFont(GAME_FONT);
	leader_board_label.setString("Leader board");
	leader_board_label.setFillColor(DARK_COLOR);
	leader_board_label.setCenterPosition(sf::Vector2f(3*WINDOW_WIDTH / 4, 50));
	m_widgets.insert(std::make_shared<Text>(leader_board_label));

	auto leader_board = std::make_shared<Leaderboard>(sf::Vector2f(20 + 1160 / 2, 80), sf::Vector2f(1160/2, 450));
	for (unsigned int i = 0; i < document["leaderboard"].Size(); i++)
	{
		Leaderboard::Entry entry;
		entry.username = document["leaderboard"][i]["username"].GetString();
		entry.score = std::to_string(document["leaderboard"][i]["score"].GetInt());
		leader_board->addEntry(entry);
	}
	m_widgets.insert(leader_board);

	Text your_rank_label;
	your_rank_label.setFont(GAME_FONT);
	your_rank_label.setString("Your rank");
	your_rank_label.setFillColor(DARK_COLOR);
	your_rank_label.setCenterPosition(sf::Vector2f(WINDOW_WIDTH / 4, WINDOW_HEIGHT /4));
	m_widgets.insert(std::make_shared<Text>(your_rank_label));

	Text rank_label;
	rank_label.setFont(GAME_FONT);
	rank_label.setString(std::to_string(document["rank"].GetInt()));
	rank_label.setFillColor(DARK_COLOR);
	rank_label.setCharacterSize(40);
	rank_label.setCenterPosition(sf::Vector2f(WINDOW_WIDTH / 4, WINDOW_HEIGHT / 4 + 100));
	m_widgets.insert(std::make_shared<Text>(rank_label));


	Button button("Exit", sf::Vector2f(500, 590), sf::Vector2f(200, 50));
	button.bindCommand(std::bind(&GameApplication::switchToMainMenu, m_game));
	button.bindSound(CLICK_SOUND);
	button.bindKey(sf::Keyboard::Escape);
	m_widgets.insert(std::make_shared<Button>(button));

	m_game->setMusic(SCOREBOARD_MUSIC_PATH);
}

void Scoreboard::exit()
{
	GameState::exit();
	m_game->setMusic(MAIN_MENU_MUSIC_PATH);
}

OptionMenu::OptionMenu(GameApplication* game) :
	GameState(game)
{
}

OptionMenu::~OptionMenu()
{
}

void OptionMenu::init()
{
	GameState::init();
}

void OptionMenu::handleEvent(const sf::Event& event)
{
	GameState::handleEvent(event);

	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Escape:
			m_game->switchToMainMenu();
			break;
		}
		break;
	}
}

GameApplication::GameApplication():
	m_window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Endless Runner"),

	m_loading_screen(this),
	m_login_screen(this),
	m_new_account_page(this),
	m_main_menu(this),
	m_main_scene(this),
	m_game_over_screen(this),
	m_scoreboard(this),
	m_option_menu(this),

	m_bio(nullptr),
	m_context(nullptr),
	m_ssl(nullptr),

	m_current_state(&m_loading_screen)
{
	m_window.setFramerateLimit(FRAME_RATE);

	SSL_load_error_strings();
	SSL_library_init();

	m_loading_screen.init();
	m_music.setLoop(true);
}

GameApplication::~GameApplication()
{
	SSL_CTX_free(m_context);
	BIO_free_all(m_bio);
}

void GameApplication::init()
{
}

void GameApplication::run()
{
	m_window.setVisible(true);

	while (m_window.isOpen())
	{
		float dt = m_clock.getElapsedTime().asSeconds();
		m_clock.restart();

		sf::Event event;
		while (m_window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			default:
				break;
			}

			m_current_state->handleEvent(event);
		}

		m_window.clear(CLEAR_COLOR);
		m_current_state->update(dt);
		m_window.display();
	}
}

bool GameApplication::send(const char* packet)
{
	return BIO_puts(m_bio, packet) > 0;
}

bool GameApplication::receive(char* packet, unsigned int size, unsigned int& received)
{
	memset(packet, 0, size);
	int n = BIO_read(m_bio, packet, size);
	if (n <= 0) return false;
	received = static_cast<unsigned int>(n);
	return true;
}

bool GameApplication::updateScore(unsigned int score)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	writer.StartObject();
	writer.Key("new_score"); writer.Uint(score);
	writer.EndObject();

	std::string request = "UPDATESCORE " + m_token + "\n" + std::string(buffer.GetString());
	std::cout << request << std::endl;

	bool status = send(request.c_str());

	if (!status)
	{
		std::cerr << "failed to send the score" << std::endl;
		return false;
	}

	unsigned int size;
	char response[100];

	status = receive(response, 100, size);

	if (!status)
	{
		std::cerr << "Failed to reveive the data" << std::endl;
		return false;
	}


	std::string s_response(response, size);
	std::vector<std::string> ss_response = splitString(s_response, "\n");

	if (ss_response[0] != "OK" || ss_response.size() != 2)
	{
		std::cout << "Server side fail" << std::endl;
		std::cout << s_response << std::endl;
		return false;
	}

	rapidjson::Document document;
	document.Parse(ss_response[1].c_str());

	m_game_over_screen.init(score, document["best"].GetInt() == 1);
	return true;
}

void GameApplication::setMusic(const std::string& filepath)
{
	m_music.stop();
	m_music.openFromFile(filepath);
	m_music.play();
}

void GameApplication::switchTo(GameState* new_state)
{
	m_current_state->exit();
	m_current_state = new_state;
	m_current_state->init();
}

void GameApplication::switchToMainMenu()
{
	switchTo(&m_main_menu);
}

void GameApplication::switchToMainScene()
{
	switchTo(&m_main_scene);
}

void GameApplication::switchToGameOverScreen(unsigned int score)
{
	switchTo(&m_game_over_screen);
}

void GameApplication::switchToScoreBoard()
{
	switchTo(&m_scoreboard);
}

void GameApplication::switchToOptionMenu()
{
	switchTo(&m_option_menu);
}

void GameApplication::switchToNewAccountPage()
{
	switchTo(&m_new_account_page);
}

void GameApplication::switchToLoginScreen()
{
	m_token = "";
	switchTo(&m_login_screen);
}