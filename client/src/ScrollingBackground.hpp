#pragma once

#include <memory>
#include <deque>
#include <functional>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "ResourceManager.hpp"
#include "Obstacle.hpp"


class ScrollingBackground : public Object
{
public:
	ScrollingBackground();

	void update(float);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	static bool firstCriteria(float, float);

	static float calculateXmin(float, float, float);
	static float calculateXmax(float, float, float, float);
	static float calculateJumpLenght(float);

	unsigned int numHiddenObstacles() const;
	void generateNextObstacle();

	float m_difficulty;

	std::deque<std::shared_ptr<Obstacle>> m_obstacles;
};

