#include "Animation.hpp"


FrameSeries::FrameSeries()
{
}

FrameSeries::FrameSeries(const sf::IntRect& initial_frame, const sf::Vector2i& translation_vector, unsigned int num_frames)
{
	this->reserve(num_frames);

	sf::Transform translation;
	translation.translate(translation_vector.x, translation_vector.y);

	sf::FloatRect frame = static_cast<sf::FloatRect>(initial_frame);

	for (unsigned int i = 0; i < num_frames; i++)
	{
		this->push_back(static_cast<sf::IntRect>(frame));
		frame = translation.transformRect(frame);
	}
}

FrameSeries merge(FrameSeries left, const FrameSeries& right)
{
	left.insert(left.end(), right.begin(), right.end());
	return left;
}



Animation::Animation(ResourceManager::TextureID texture, const FrameSeries& frame_series, const std::vector<ResourceManager::ImageID>& ram_frames) :
	m_texture(texture),
	m_frame_series(frame_series),
	m_RAM_frames(ram_frames),
	m_counter(0),
	m_elapsed_time(0),
	m_framerate_inv(0),
	m_reached_end(false)
{
}

void Animation::setFrameRate(float f)
{
	m_framerate_inv = 1 / f;
}

void Animation::start()
{
	m_reached_end = false;
	m_elapsed_time = 0;
	m_counter = 0;

	m_sprite.setTexture(*ResourceManager::getTexture(m_texture));
	m_sprite.setTextureRect(m_frame_series.front());
}

void Animation::stop()
{
	m_reached_end = true;
}

const sf::Sprite& Animation::update(float dt)
{
	if (m_counter >= m_frame_series.size() - 1)
	{
		m_reached_end = true;
		return m_sprite;
	}

	m_elapsed_time += dt;

	//std::cout << m_frame_series.size() << std::endl;

	if (m_elapsed_time > m_framerate_inv)
	{
		m_elapsed_time = 0;
		m_counter++;

		m_sprite.setTextureRect(m_frame_series[m_counter]);
	}

	return m_sprite;
}

bool Animation::reachedEnd() const
{
	return m_reached_end;
}

const sf::Sprite& Animation::getCurrentFrame() const
{
	return m_sprite;
}

ResourceManager::ImageID Animation::getCurrentRAMFrame() const
{
	return m_RAM_frames[m_counter];
}