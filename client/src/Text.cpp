#include "Text.hpp"


Text::Text()
{
}

Text::Text(const sf::String& string, const sf::Font& font, unsigned int charactersize):
	m_text(string, font, charactersize)
{
}

Text::~Text()
{
}

void Text::setPosition(const sf::Vector2f& position)
{
	Widget::setPosition(position);
	m_text.setPosition(position);
}

void Text::setCenterPosition(const sf::Vector2f& position)
{
	setPosition(position - sf::Vector2f(getLocalBounds().width / 2, getGlobalBounds().height / 2));
}

void Text::setFont(ResourceManager::FontID font) 
{ 
	m_text.setFont(*ResourceManager::getFont(font)); 
}

void Text::handleEvent(const sf::Event& event)
{
}

void Text::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_text, states);
}