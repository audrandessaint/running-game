#pragma once

#include <vector>

#include "Object.hpp"
#include "ResourceManager.hpp"


class Player;


class Collidable : public Object
{
public:
	Collidable();
	Collidable(sf::Sprite, ResourceManager::ImageID);
	virtual ~Collidable();

	void operator=(const Collidable&) = delete;

	static bool globalCollisionTest(const Player&);

protected:
	ResourceManager::ImageID m_image_id;

private:
	bool collides(const Collidable&) const;

	static std::vector<Collidable*> m_collidables;
};

