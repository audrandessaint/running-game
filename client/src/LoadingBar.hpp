#pragma once

#include "Widget.hpp"


class LoadingBar : public Widget
{
public:
	LoadingBar(unsigned int, unsigned int, float = 0.f);
	~LoadingBar();

	void setProgress(float);
	void update(float);

	float getProgress() const { return m_progress; }

	virtual void setPosition(const sf::Vector2f&);

	virtual void handleEvent(const sf::Event&);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	float m_progress;
	unsigned int m_width;
	unsigned int m_height;
	sf::ConvexShape m_border;
	sf::ConvexShape m_shape;
};


// voir si doit �tre impl�menter mais loin d'�tre une priorit�
// si impl�ment�, remettera peut-�tre en question toute
// l'architecture de l'UI pour la rendre plus interactive
// en lui permettant de prendre en compte le temps dans 
// son affichage

//class SmoothLoadingBar : private LoadingBar, Object
//{
//public:
//
//private:
//}