from requesthandler import *
import datetime
import argon2
import pymongo

request_handler = RequestHandler()

db_client = pymongo.MongoClient(settings['database']['host'], int(settings['database']['port']), username="admin", password="password")
password_hasher = argon2.PasswordHasher()


@request_handler.register('CREATEUSER')
@unprotected
def createUser(request_body):
    username = request_body['username']
    password = request_body['password']
    email = request_body['email']
    
    if db_client.shadowrunner.users.count_documents({'username': username}, limit=1):
        return errorResponse("This username already exists")
    
    password_hash = password_hasher.hash(password)
    
    try:
        user = {'username': username, 'password_hash': password_hash, 'email': email, 'score': 0}
        db_client.shadowrunner.users.insert_one(user)
    except:
        return errorResponse("Failed to write on the database")
    return successResponse()


@request_handler.register('LOGIN')
@unprotected
def login(request_body):
    username = request_body['username']
    password = request_body['password']
    
    try:
        user = db_client.shadowrunner.users.find_one({'username': username}, {'password_hash': 1})
        password_hasher.verify(user['password_hash'], password)
    except:
        return errorResponse("Invalid password")
    
    token = jwt.encode({'user_id': str(user['_id']), 
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=int(settings['security']['token_life_span']))
                        },
                       settings['security']['private_key'], settings['security']['jwt_algo'])
    return successResponse({'token': token})
    

@request_handler.register('UPDATESCORE')
@protected
def updateUserScore(user_id, request_body):
    new_score = request_body['new_score']
    try:
        user = db_client.shadowrunner.users.find_one({'_id': user_id}, projection={'score': 1})
        if user['score'] < new_score:
            db_client.shadowrunner.users.update_one({'_id': user_id}, update={"$set": {'score': new_score}})
            return successResponse({'best': 1})
        return successResponse({'best': 0})
    except Exception as e:
        print(e)
        return errorResponse("Unexpected error while writting in the database")
    

@request_handler.register('SCOREBOARD')
@protected
def getScoreboard(user_id, request_body):
    try:
        score = db_client.shadowrunner.users.find_one({'_id': user_id}, projection={'score': 1})['score']
        rank = 1 + db_client.shadowrunner.users.count_documents({'score': {'$gt': score}})

        leader_board = db_client.shadowrunner.users.find(
            projection={
                '_id': 0,
                'username': 1,
                'score': 1
                }
            ).sort([('score', pymongo.DESCENDING), ('_id', pymongo.ASCENDING)]).limit(int(settings['game']['leaderboard_size']))
        score_board = {'rank': rank, 'leaderboard': list(leader_board)}
        return successResponse(score_board)
    except Exception as e:
        print(e)
        return errorResponse("Unexpected error while reading in the database")
