import socketserver
import ssl
import configparser

from servercommands import request_handler


settings = configparser.ConfigParser()
settings.read("./settings.ini")

PORT = int(settings['network']['port'])
SSL_PROTOCOL = ssl.PROTOCOL_TLS_SERVER
CERT_PATH = settings['security']['ssl_certificate_path']
KEY_PATH = settings['security']['ssl_key_path']


class Server(socketserver.ThreadingMixIn, socketserver.TCPServer):
    
    def __init__(self, address, port):
        socketserver.TCPServer.__init__(self, (address, port), request_handler)
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.server_close()
        return super().__exit__(exc_type, exc_val, exc_tb)
        
    def get_request(self):
        socket, addr = super().get_request()
        print("[DEBUG] connexion attempt") #tmp
        
        context = ssl.SSLContext(SSL_PROTOCOL)
        context.load_cert_chain(CERT_PATH, KEY_PATH, "Olga$nmqdR")
        ssocket = context.wrap_socket(socket, server_side=True)
        
        return ssocket, addr
    
    def run(self): 
        print("The server is listening on port {}".format(self.server_address[1]))
        self.serve_forever()
        
        
if __name__ == '__main__':
   
    with Server("", PORT) as server:
        server.run()
