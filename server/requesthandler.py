import socketserver
import functools
import json
import bson
import jwt
import configparser


settings = configparser.ConfigParser()
settings.read("./settings.ini")


def errorResponse(error_message):
    return "FAIL\n" + json.dumps({'error': error_message})

def successResponse(response = None):
    if response:
        return "OK\n" + json.dumps(response)
    return "OK"
    

class RequestHandler(socketserver.BaseRequestHandler):
    
    def __init__(self):
        self.registered_requests = {}
        self.max_packet_size = int(settings['network']['max_packet_size'])
            
    def __call__(self, *args, **kwargs):
        socketserver.BaseRequestHandler.__init__(self, *args, **kwargs)
        return self
        
    def register(self, request_type):
        def decorator(function):
            assert not request_type in self.registered_requests
            self.registered_requests[request_type] = function
            return function
        return decorator
    
    def parseRequest(self, request):
        splited_request = request.split('\n', 1)

        header = splited_request[0]
        splited_header = header.split(' ')
        request_type = splited_header[0]

        if len(splited_header) == 2:
            token = splited_header[1]
        else:
            token = None
        
        if len(splited_request) == 2:
            body = json.loads(splited_request[1])
        else:
            body = ""
            
        return request_type, token, body
        
    def handle(self):
        while True:
            try:
                request = self.request.recv(self.max_packet_size).decode('utf-8')
            except ConnectionResetError:
                print("[DEBUG] Connection reset")
                return
            
            print("[DEBUG] Request: " + request)
            request_type, token, body = self.parseRequest(request)
            
            if request_type in self.registered_requests:
                response = self.registered_requests[request_type](token, body)
            else:
                response = errorResponse("Unknown request")
            
            try:
                # can throw an exception if the client disconnect
                print("[DEBUG] Response: " + response)
                self.request.sendall(response.encode('utf-8'))
            except : #find the right exception type
                print("[DEBUG] Failed to send the data to client, end of connection")
                return
            
        
        
def protected(request_handling_function):
    @functools.wraps(request_handling_function)
    def wrapper(token, request_body):
        if not token:
            return errorResponse("Missing token")
        
        try:
            token_payload = jwt.decode(token, settings['security']['private_key'], settings['security']['jwt_algo'])
            user_id = bson.ObjectId(token_payload['user_id'])
        except:
            return errorResponse("Invalid token")
        
        return request_handling_function(user_id, request_body)
    return wrapper

# Dummy decorator to explicitly declare that functions are not protected
# (it is not possible to create a callback function for handling requests
# that is neither protected nor unprotected)
def unprotected(request_handling_function):
    @functools.wraps(request_handling_function)
    def wrapper(token, request_body): # unused token on purpose
        return request_handling_function(request_body)
    return wrapper