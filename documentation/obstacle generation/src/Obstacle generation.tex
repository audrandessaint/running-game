\documentclass{article}

\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{titling}
\usepackage{graphicx}
\usepackage{amsfonts}

\setlength{\droptitle}{-1.9cm}

\title{Génération procédurale d'obstacles franchissables}
\author{Audran Dessaint}


\begin{document}

\maketitle

\subsection{Équation du saut}

L'application de la deuxième loi de Newton sur le personnage conduit au système d'équations suivant régissant la dynamique du saut :

\begin{equation}
    \begin{cases}
      x(t) = v_x t + x_0\\
      y(t) = -\frac{1}{2} g t^2 + v_{j0} t
    \end{cases}
\end{equation}

\noindent
avec :

\begin{itemize}
	\item $v_x$ la vitesse de déplacement horizontale du personnage
	\item $x_0$ la position initiale
	\item $v_{j0}$ la vitesse verticale initiale
	\item $g$ la gravité
\end{itemize}

\subsection{Longueur du saut}

On a $\Delta x = v_x \Delta t$ avec $\Delta t = t_2 - t_1$ où $t_2$ et $t_1$ sont les racines de $y$ (ie. les instants où le personnage quitte le sol puis le retrouve). D'où :

\begin{equation}
\Delta x = 2 \frac{v_x v_{j0}}{g}
\end{equation}

\subsection{Conditions de franchissabilité}

%\begin{figure}[h!]
%\centering
%\includegraphics[scale=0.6]{obstacle.png}
%\end{figure}

On considère un obstacle rectangulaire dont les sommets supérieurs sont notés A et B et on suppose que le personnage est représenté par un rectangle de largeur (selon x) notée $w$. Les abscisses minimale et maximale pour lesquelles sauter permet de franchir l'obstacle, notée respectivement $x_{min}$ et $x_{max}$, s'écrivent :

\begin{equation}
x_{min} = B_x - \frac{v_x v_{j0}}{g}\left(1+\sqrt{1-2\frac{g B_y}{v_{j0}^2}}\right)
\end{equation}

\begin{equation}
x_{max} = A_x - w - \frac{v_x v_{j0}}{g}\left(1-\sqrt{1-2\frac{g A_y}{v_{j0}^2}}\right)
\end{equation}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{obstacle2.png}
\caption{L'abscisse du point E est $x_{min}$, celle du point G est  $x_{max}$ et la longueur GF est $w$.}
\end{figure}

\subsubsection{Premier critère de franchissabilité}

La résolution des équations menant aux résultats de la partie précédente introduit la condition suivante :

\begin{equation}
v_{j0} > \sqrt{2\mbox{max}(A_y, B_y)g}
\end{equation}

\noindent
ou de manière équivalente :

\begin{equation}
\mbox{max}(A_y, B_y) < \frac{v_{j0}^2}{2g}
\end{equation}

\noindent
en fonction de si l'on cherche une condition sur $\mbox{max}(A_y, B_y)$ plutôt que sur $v_{j0}$. De plus, la deuxième formulation s'évalue plus rapidement par un ordinateur.

Lorsque cette condition n'est pas respectée, cela signifie que le personnage ne saute pas assez haut pour pouvoir franchir l'obstacle.

\subsubsection{Second critère de franchissabilité}

La deuxième condition pour que l'obstacle puisse être franchi est évidemment :

\begin{equation}
x_{min} < x_{max}
\end{equation}

Cette condition permet de calculer à partir de quelle vitesse horizontale un obstacle donné peut être franchi :

\begin{equation}
v_x > \frac{g(B_x + w - A_x)}{\sqrt{v_{j0}^2 - 2 g A_{y}} + \sqrt{v_{j0}^2 - 2 g B_{y}}}
\end{equation}

\subsection{Étude de l'enchainement de sauts}

\subsubsection{Cas de deux obstacles}

Une succession de deux obstacles est franchissable si et seulement si :

\begin{equation}
x_{1,min} + \Delta x < x_{2,max}
\end{equation}

\noindent
c'est à dire si la zone d'arrivée lorsque l'on franchi le premier obstacle n'est pas incluse dans la zone à partir de laquelle il n'est plus possible de franchir le deuxième obstacle.

En posant $\tilde{x}_{2,min} = \mbox{max}(x_{1,min}+\Delta x, x_{2,min})$, on peut réécrire la condition sous la forme :

\begin{equation}
\tilde{x}_{2,min} < x_{2,max}
\end{equation}

\noindent
ce qui nous ramène à un critère de la forme de celui de l'équation (7).

\subsubsection{Généralisation au franchissement de multiples obstacles}

On définit :

\begin{equation}
    \begin{cases}
     \tilde{x}_{0,min} = \mbox{max}(x_{0,min}, x_{\mbox{perso}})\\
     \forall n \in \mathbb{N}, \tilde{x}_{n+1,min} = \mbox{max}(\tilde{x}_{n,min} + \Delta x, x_{n+1, min})
    \end{cases}
\end{equation}

Pour vérifier qu'une suite de $N$ obstacles est franchissable (en supposant que les vitesses verticales et horizontales conviennent cf. critères de la partie 3), il suffit alors de vérifier la propriété suivante :

\begin{equation}
\forall n \in \lbrace 0, 1, ..., N-1\rbrace,  \tilde{x}_{n,min} < x_{n,max}
\end{equation}

Si cette condition n'est pas respectée alors $\mbox{min}\lbrace i \leq N-1 \vert \tilde{x}_{i,min} < x_{i,max}\rbrace$ est l'indice de l'obstacle qu'il sera impossible pour le personnage de dépasser.

Il est alors clair que si l'on souhaite ajouter un obstacle à une suite de $N$ obstacles de manière à préserver le critère de franchissabilité, le nouvel obstacle doit respecter la condition suivante :

\begin{equation}
\tilde{x}_{N,min} < x_{N, max}
\end{equation}

\end{document}