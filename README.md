# Shadow Runner

Shadow Runner is a 2D competitive endless runner game.

https://audran-dessaint.itch.io/shadow-runner

# ! DISCLAIMER !

This code is far from production reading as it contains temporary credentials and do not rely on AWS secrets yet.
Besides, the **main** branch is way behind the **dev** branch and does not take into account the port to AWS.
